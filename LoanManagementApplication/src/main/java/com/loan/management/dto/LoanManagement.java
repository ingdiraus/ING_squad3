package com.loan.management.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="loanmanagement")
public class LoanManagement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "loan_id")
	private int loanId;
	
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "address")
	private String address;
	@Column(name = "credit_status")
	private String creditStatus;
	@Override
	public String toString() {
		return "LoanManagement [loanId=" + loanId + ", firstName=" + firstName + ", lastName=" + lastName + ", address="
				+ address + ", creditStatus=" + creditStatus + ", applicationIncome=" + applicationIncome
				+ ", loanAmount=" + loanAmount + ", timeCreated=" + timeCreated + "]";
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCreditStatus() {
		return creditStatus;
	}
	public void setCreditStatus(String creditStatus) {
		this.creditStatus = creditStatus;
	}
	public String getApplicationIncome() {
		return applicationIncome;
	}
	public void setApplicationIncome(String applicationIncome) {
		this.applicationIncome = applicationIncome;
	}
	public String getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
	@Column(name = "application_income")
	private String applicationIncome;
	@Column(name = "loan_amount")
	private String loanAmount;
	@Column(name = "time_created")
	private String timeCreated;
	
	
}
