package com.stock.management.service;

import java.io.IOException;

import com.stock.management.dto.StockList;
import com.stock.management.dto.StockQuotes;

public interface StockMangementService {

	public void insertStockData(StockList stockList );
	public Iterable<StockList> getStockData();
	public void deleteStockId(long stockId);
	public void updateCalculatedPrice(String userName,long stockId, long quantity);
	public  StockList  getStockLiveData( String symbol)throws IOException ; 
}
