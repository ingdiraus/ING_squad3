package com.stock.management.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.management.dao.StockMangementDao;
import com.stock.management.dto.StockList;
import com.stock.management.dto.StockQuotes;
import com.stock.management.service.StockMangementService;

@Service
public class StockMangementServiceImpl implements StockMangementService{
	@Autowired
	private StockMangementDao stockMangementDao;
	@Override
	public void insertStockData(StockList stockList) {
		 
		stockMangementDao.insertStockData(stockList);
	}

	@Override
	public Iterable<StockList> getStockData() {
		 
		return stockMangementDao.getStockData();
	}

	@Override
	public void deleteStockId(long stockId) {
		// TODO Auto-generated method stub
		stockMangementDao.deleteStockId(stockId);
	}

	@Override
	public void updateCalculatedPrice(String userName,long stockId, long quantity) {
		// TODO Auto-generated method stub
		stockMangementDao.updateCalculatedPrice( userName, stockId,  quantity);
	}

	@Override
	public  StockList  getStockLiveData( String symbol) throws IOException{
		// TODO Auto-generated method stub
		return stockMangementDao.getStockLiveData(symbol);
	}

	 

}
