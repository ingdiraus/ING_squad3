package com.stock.management.controlller;

 
import java.io.IOException;

import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stock.management.dto.StockList;
import com.stock.management.service.StockMangementService;

@RestController 
public class StockManagementController {
	@Autowired
	private StockMangementService stockMangementService;
	
	/*@RequestMapping(value = "/insertdata", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON)
	 
	public void  insertStockData(@RequestBody StockList stockList){
		 System.out.println(stockList.toString());
		stockMangementService.insertStockData(stockList);
	}*/
	@RequestMapping(value = "/getdata", method = RequestMethod.GET)
	@Produces(MediaType.APPLICATION_JSON)
	public Iterable<StockList>  getStockData( ){
		  
		return stockMangementService.getStockData();
	}
	@RequestMapping(value = "/deletestockid", method = RequestMethod.GET)
	public void deleteStockId(@QueryParam("stockId") long stockId){
		stockMangementService.deleteStockId(stockId);
		
	}
	@RequestMapping(value = "/updatecalprice", method = RequestMethod.PUT)
	public void updateCalculateprice(@QueryParam("userName") String userName,@QueryParam("stockId") long stockId,@QueryParam("quantity") long quantity){
		stockMangementService.updateCalculatedPrice(userName,stockId,quantity);
		
	}
	
	@RequestMapping(value = "/getLiveData", method = RequestMethod.GET)
	@Produces(MediaType.APPLICATION_JSON)
	public  StockList getStockLiveData( @QueryParam("symbol") String symbol)throws IOException {
		  
		return stockMangementService.getStockLiveData(symbol);
	}
}
