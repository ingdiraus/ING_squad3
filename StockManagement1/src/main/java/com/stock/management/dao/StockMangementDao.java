package com.stock.management.dao;

import java.io.IOException;

import com.stock.management.dto.StockList;
import com.stock.management.dto.StockQuotes;

public interface StockMangementDao {
	public void insertStockData(StockList stockList );
	public Iterable<StockList> getStockData();
	
	public  StockList  getStockLiveData( String symbol) throws IOException;
	public void deleteStockId(long stockId);
	public void updateCalculatedPrice(String userName,long stockId,long calculatedPrice);
}
