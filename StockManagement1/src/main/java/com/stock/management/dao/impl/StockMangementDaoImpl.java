package com.stock.management.dao.impl;

import java.io.IOException;
import java.math.BigDecimal;

import javax.xml.bind.ParseConversionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stock.management.config.StockManagementRepostory;
import com.stock.management.dao.StockMangementDao;
import com.stock.management.dto.StockList;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.stock.StockQuote;

@Repository
public class StockMangementDaoImpl implements StockMangementDao {

	 
	@Autowired
	private StockManagementRepostory stockManagementRepostory;

	@Override
	public void insertStockData(StockList stockList) {

		stockManagementRepostory.save(stockList);

	}

	@Override
	public Iterable<StockList> getStockData() {
		return stockManagementRepostory.findAll();
	}

	@Override
	public void deleteStockId(long stockId) {

		stockManagementRepostory.delete(stockId);
	}

	@Override
	public void updateCalculatedPrice(String userName,long stockId, long quantity) {
		StockList stockList= stockManagementRepostory.findOne(stockId);
		double calprice = 0;
		double charges = 0.10;
		BigDecimal sprice=stockList.getStockUnitPrice();
		
		String s = sprice.toString(); 
		double d = Double.parseDouble(s);
		double totalprice=d*quantity;
		if(quantity<500){
			 
			calprice=d*10/100;
			 
		}  else if(quantity>500||quantity==500){
			calprice=d*15/100;
		}
		double totalprice1=totalprice+calprice;
		
		// stockManagementRepostory.save(arg0);

	}

	@Override
	public  StockList  getStockLiveData(String symbol) throws IOException   {
    System.out.println("symbol"+symbol);
		YahooFinance finance = new YahooFinance();
		//Stock stock1  = finance.get(symbol.trim()); 
		Stock stock1  = finance.get(symbol.trim());
		StockQuote sq = stock1.getQuote();
		sq.getPrice();
		sq.getSymbol();
		StockList stockList = new StockList();
		stockList.setStockName(sq.getSymbol());
		stockList.setStockUnitPrice(sq.getPrice());
		System.out.println("stockList"+stockList.toString());
		
		stockManagementRepostory.save(stockList);
		//list.add(stockQuotes);
		
		/*// ZH0LOT45QS86IG6L
		String url = "https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=MSFT&apikey=ZH0LOT45QS86IG6L";

		String LIVE_STOCK_URL = "https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=" + symbol
				+ "&apikey=ZH0LOT45QS86IG6L";
		System.out.println("url:" + LIVE_STOCK_URL);
		RestTemplate restTemplate = new RestTemplate();
		String jsondata = restTemplate.getForObject(url, String.class);
		List<StockQuotes> list = new ArrayList<StockQuotes>();
		JSONObject jsonObject = new JSONObject(jsondata);
		JSONArray stockjsonArray = jsonObject.getJSONArray("Stock Quotes");
		if (stockjsonArray != null) {
			for (int i = 0; i < stockjsonArray.length(); i++) {
				JSONObject jsonObject2 = stockjsonArray.getJSONObject(i);
				StockQuotes stockQuotes = new StockQuotes();
				stockQuotes.setSymbol(jsonObject2.getString("1. symbol"));
				stockQuotes.setPrice(jsonObject2.getLong("2. price"));
				stockQuotes.setPrice(jsonObject2.getLong("3. volume"));
				list.add(stockQuotes);

			}
		} else {
			return stockMangementDao.getStockData();
			
		}*/
		return stockList;
	}

}
