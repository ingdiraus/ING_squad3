package com.stock.management.dto;

import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name="stocklist")
public class StockList {
	
	@Override
	public String toString() {
		return "StockList [stockId=" + stockId + ", stockName=" + stockName + ", stockUnitPrice=" + stockUnitPrice
				+ "]";
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "stock_id")
	private int stockId;
	@Column(name = "stock_name")
	private String stockName;
	@Column(name = "stock_price")
	private BigDecimal stockUnitPrice;
	public int getStockId() {
		return stockId;
	}
	public BigDecimal getStockUnitPrice() {
		return stockUnitPrice;
	}
	public void setStockUnitPrice(BigDecimal stockUnitPrice) {
		this.stockUnitPrice = stockUnitPrice;
	}
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	 
	 
	
	
}
