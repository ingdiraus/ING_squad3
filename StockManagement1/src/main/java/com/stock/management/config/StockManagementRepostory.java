package com.stock.management.config;

 

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stock.management.dto.StockList;
@Repository
public interface  StockManagementRepostory  extends CrudRepository<StockList, Long>{
	
 
}
